import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Navigation from './components/navigation';
import Avatar from './components/avatar';
import gitlabLogo from './gitlabLogo.svg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Avatar />
        <Navigation />
        <Header />
        <div>
          <a href="https://gitlab.com/partha360/portfolio" target="_blank" rel="noopener noreferrer">
            <span className="gitlabText">Get this source from&nbsp;
              <img src={gitlabLogo} alt="Gitlab Source" className="gitlabLogo" />
            </span>
          </a>
        </div>
      </div>
    );
  }
}

export default App;
