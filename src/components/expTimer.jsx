import React, { Component } from 'react';

class ExperianceTimer extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentWillMount() {
    clearInterval(this.timerID);
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(), 1000
    );
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    return (
      <div>
        <strong>
          <span className="year">{this.state.date.getFullYear() - 2001}y </span>
          <span className="month">{this.state.date.getMonth()}mo </span>
          <span className="day">{this.state.date.getDate()}d </span>
          <span className="min">{this.state.date.getMinutes()}m </span>
          <span className="seconds">{this.state.date.getSeconds()}s </span>
        </strong>
                in IT,
      </div>
    );
  }
}

export default ExperianceTimer;
