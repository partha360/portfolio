import React, { Component } from 'react';
import parthaLogo from '../parthaLogo.svg';
import ExperianceTimer from './expTimer';
import Partha from '../images/Partha.jpg';

class Avatar extends Component {
  render() {
    return (
      <div className="tooltip left">
        <div className="container-Avatar">
          <img src={parthaLogo} alt="Avatar" className="grayscale-Avatar" />
          <div className="overlay-Avatar">
            <img className="org-Avatar" alt="logo" src={Partha} />
          </div>
          <div className="content">
            <ExperianceTimer />
            <hr />
            <span>Making Digital Real & Helping Business Thrive through right Technology Solutions</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Avatar;
